Name:      lscsoft-testing-debug-config
Version:   8
Release:   2.1%{?dist}
Summary:   Repository configuration for LSCSoft Testing Debug

Group:     System Environment/Base
License:   GPLv3+
Source0:   lscsoft-testing-debug.repo
BuildArch: noarch
Requires:  redhat-release >= %{version}

%description
Repository configuration for LSCSoft Testing Debug

%prep
%setup -q -c -T

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0,root,root,-)
%config /etc/yum.repos.d/*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Mon Aug 02 2021 Adam Mercer <adam.mercer@ligo.org> 8-2.1
- actually bump revision

* Mon Aug 02 2021 Adam Mercer <adam.mercer@ligo.org> 8-2
- update URL for Rocky Linux

* Fri Apr 03 2020 Adam Mercer <adam.mercer@ligo.org> 8-1
- initial release
